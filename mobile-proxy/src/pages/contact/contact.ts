import { Component } from '@angular/core';
import {NavController, Platform} from 'ionic-angular';
import {BluetoothSerial} from "@ionic-native/bluetooth-serial/ngx";
import {Toast} from "@ionic-native/toast/ngx";

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  constructor(public navCtrl: NavController,
              private bluetoothSerial: BluetoothSerial,
              private toast: Toast,
              public plt: Platform

  ) {
    this.plt.ready().then((readySource) => {
      console.log('dara', readySource);
      this.getAllBluetoothDevices();
    })
  }

  getAllBluetoothDevices() {
    // this.bluetoothSerial.isConnected().then(data => {
    //   this.isConnect = JSON.stringify(data);
    // });
    // async so keep everything in this method
    this.bluetoothSerial.isEnabled().then((data) => {
      // not sure of returning value, probably a boolean
      this.bluetoothSerial.connect('98:D3:91:FD:3A:36').subscribe(dat => {
        this.toast.show('Connected to device', '5000', 'center').subscribe(
          toast => {
            console.log(toast);
          }
        );
        this.bluetoothSerial.subscribeRawData().subscribe(
          data => {
       console.log(data);
          }
        )
      });
    });
  }
}
