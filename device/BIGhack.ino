#include <DHT.h>
#include <DHT_U.h>
#include <SoftwareSerial.h>

SoftwareSerial hc06(3,4);

#define DHTPIN 2     

#define DHTTYPE DHT11   

DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(9600);
  dht.begin();
}

void loop() {
  
  delay(7000);
  float h = dht.readHumidity();
  float t = dht.readTemperature();
  float f = dht.readTemperature(true);

  if (isnan(h) || isnan(t) || isnan(f)) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }

  float hif = dht.computeHeatIndex(f, h);
  float hic = dht.computeHeatIndex(t, h, false);

  Serial.print("{\"h\":");
  Serial.print(h);
  Serial.print(",");
  Serial.print("\"t\":");
  Serial.print(t);
  Serial.print("}");

}
